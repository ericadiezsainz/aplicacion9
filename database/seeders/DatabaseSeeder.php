<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void {
        User::factory()->create([
            'name' => 'aaron',
            'email' => 'aaron@alpe.com',
            'password' => Hash::make('Usuario@1'),
        ]);
        User::factory()->create([
            'name' => 'ramon',
            'email' => 'ramon@alpe.com',
        ]);
        User::factory()->create([
            'name' => 'luisa',
            'email' => 'luisa@alpe.com',
        ]);
        $this->call([
            NoticiaSeeder::class
        ]);
    }
}
