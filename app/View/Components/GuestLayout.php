<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

/**
 * Este componente es el layout para las vistas de registro, login
*/
class GuestLayout extends Component {
    /**
     * Get the view / contents that represents the component.
     */
    public function render(): View {
        return view('layouts.guest');
    }
}
