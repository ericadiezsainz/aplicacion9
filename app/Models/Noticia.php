<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Schema;

class Noticia extends Model {
    use HasFactory;

    // protected $table = 'noticias';

    protected $fillable = [
        'titulo',
        'contenido',
        'foto',
        'user_id',
    ];

    public static $labels = [
        'titulo' => 'Título',
        'contenido' => 'Contenido',
        'foto' => 'Foto',
        'user_id' => 'Usuario',
        // 'nombreuser' => 'Nombre',
    ];

    /**
     ** Recupera la lista de campos de la tabla asociada al modelo.
     *
     * @return array La lista de nombres de campos.
    */
    public function getFields(): array {
        return Schema::getColumnListing($this->table);
    }

    /**
     ** Recupera la etiqueta de un atributo dado.
     *
     * @param string $attribute El nombre del atributo.
     * @return string La etiqueta del atributo, o el nombre del atributo si no se encuentra una etiqueta.
    */
    public function getAttributeLabel($attribute): string {
        return self::$labels[$attribute] ?? $attribute;
    }

    public function user() : BelongsTo {
        return $this->belongsTo(User::class);
    }

    public function getNombreuserAttribute(): string {
        return $this->user->name;
    }
}
