<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoticiaStoreRequest;
use App\Http\Requests\NoticiaUpdateRequest;
use App\Models\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NoticiaController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $noticias = Noticia::all();
        return view('back.noticias.index', compact('noticias'));
    }

    /**
     * Show the form for creating a new resource.
    */
    public function create() {
        return view('back.noticias.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NoticiaStoreRequest $request) {
        $fotoEnviada = $request->file('foto');

        if ($fotoEnviada) {
            $foto = $fotoEnviada->store('fotos', 'public');
        } else {
            $foto = null;
        }

         $noticia = new Noticia();
        $noticia->fill($request->validated()); // Usar datos validados
        $noticia->foto = $foto;
        $noticia->user_id = Auth::id(); // Asignar el ID del usuario autenticado
        $noticia->save();

        return redirect()
            ->route('back.noticias.listado')
            ->with('mensaje', 'Noticia se ha creado con éxito');
    }

    /**
     * Display the specified resource.
     */
    public function show(Noticia $noticia) {
        return view('back.noticias.show', compact('noticia'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Noticia $noticia) {
        return view('back.noticias.edit', compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(NoticiaUpdateRequest $request, Noticia $noticia) {
        if ($request->hasFile('foto')) {
            $fotoEnviada = $request->file('foto');
            Storage::disk('public')->delete($noticia->foto);

            $nuevaFoto = $fotoEnviada->store('fotos', 'public');
            $noticia->fill($request->validated());
            $noticia->foto = $nuevaFoto;
        } else {
            $noticia->fill($request->validated());
        }

        $noticia->save();

        return redirect()
            ->route('back.noticias.listado')
            ->with('mensaje', 'Noticia actualizada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Noticia $noticia) {
        Storage::disk('public')->delete($noticia->foto);
        $noticia->delete();

        return redirect()
            ->route('back.noticias.listado')
            ->with('mensaje', 'Noticia borrada con éxito');
    }

    public function listado() {
        $noticias = Noticia::where('user_id', Auth::id())->get();
        return view('back.noticias.listado', compact('noticias'));
    }
}
