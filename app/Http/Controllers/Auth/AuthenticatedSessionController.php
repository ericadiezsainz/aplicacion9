<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller {
    /**
     * Display the login view.
    */
    public function create(): View {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
    */
    public function store(LoginRequest $request): RedirectResponse {
        $request->authenticate();

        $request->session()->regenerate();

        // Si el login es correcto me redirecciona a la vista back.index del backend
        return redirect()->intended(route('back.index', absolute: false));
    }

    /**
     * Para cerrar la sesión
    */
    public function destroy(Request $request): RedirectResponse {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        // Redireccinamos a la url '/' que es la pagina de inicio de frontend
        return redirect('/');
    }
}
