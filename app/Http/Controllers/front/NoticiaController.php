<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoticiaController extends Controller {
    /**
     * Debe mostrar todas las noticias en frontend. No debe aparecer los botones de edicion.
    */
    public function index() {
        // Obtengo todas las noticias

        // Quiero ver el nombre del usuario que ha creado la noticia
        $noticias = Noticia::all();


        // Redireciona a la vista front.index frontend
        return view('front.index', compact('noticias'));
    }

    public function show(Noticia $noticia) {
        return view('front.noticias.show', compact('noticia'));
    }
}
