<x-app-layout>
    <div class="container mx-auto p-6 mt-5 bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
        <form action="{{ route('back.noticias.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="space-y-12">
                <div class="border-b border-gray-900/10 pb-12">
                    <h2 class="text-base font-semibold leading-7 text-gray-900">Noticias</h2>
                    <p class="mt-1 text-sm leading-6 text-gray-600">Crear una noticia y lo verá todo el mundo.</p>

                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                    <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div class="sm:col-span-4">
                            <label for="titulo" class="block text-sm font-medium leading-6 text-gray-900">Título</label>
                            <div class="mt-2">
                                <div class="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                    <input type="text" name="titulo" id="titulo" placeholder="Título" class="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                </div>
                                @error('titulo')
                                    <p class="text-red-500"> {{ $message }} </p>
                                @enderror
                            </div>
                        </div>

                        <div class="col-span-full">
                            <label for="contenido" class="block text-sm font-medium leading-6 text-gray-900">Contenido</label>
                            <div class="mt-2">
                                <textarea id="contenido" name="contenido" rows="3" placeholder="Pon el contenido de la noticia" class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"></textarea>
                            </div>
                            <p class="mt-3 text-sm leading-6 text-gray-600">Escribe la noticia</p>
                        </div>
                    </div>

                    <div class="col-span-full mt-7">
                        <label for="cover-photo" class="block text-sm font-medium leading-6 text-gray-900">Cover photo</label>
                        <div class="mt-2 flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10">
                            <div class="text-center">
                                <div id="preview" class="bg-cover">
                                    <svg id="svg-icon" class="mx-auto h-12 w-12 text-gray-300" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M1.5 6a2.25 2.25 0 012.25-2.25h16.5A2.25 2.25 0 0122.5 6v12a2.25 2.25 0 01-2.25 2.25H3.75A2.25 2.25 0 011.5 18V6zM3 16.06V18c0 .414.336.75.75.75h16.5A.75.75 0 0021 18v-1.94l-2.69-2.689a1.5 1.5 0 00-2.12 0l-.88.879.97.97a.75.75 0 11-1.06 1.06l-5.16-5.159a1.5 1.5 0 00-2.12 0L3 16.061zm10.125-7.81a1.125 1.125 0 112.25 0 1.125 1.125 0 01-2.25 0z" clip-rule="evenodd" />
                                    </svg>
                                </div>
                                <div class="mt-4 flex text-sm leading-6 text-gray-600">
                                    <label for="foto" class="relative cursor-pointer rounded-md bg-white font-semibold text-indigo-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-indigo-500">
                                        <div class="flex flex-col items-center justify-center">
                                            <span>Subir Imagen</span>
                                            <p class="text-xs leading-5 text-gray-600">PNG, JPG, hasta 1MB</p>
                                        </div>
                                        <input type="file" id="foto" name="foto" class="sr-only">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-6 flex justify-center gap-x-6">
                    <button type="submit" class="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</x-app-layout>

<script>
    document.getElementById('foto').addEventListener('change', function(e) {
        var file = e.target.files[0];
        var reader = new FileReader();
        var preview = document.getElementById('preview');
        var svgIcon = document.getElementById('svg-icon');

        reader.onloadend = function() {
            preview.style.backgroundImage = 'url("' + reader.result + '")';
            preview.style.backgroundSize = 'contain';
            preview.style.backgroundRepeat = 'no-repeat';
            preview.style.backgroundPosition = 'center';
            preview.classList.add('w-64', 'h-64');
            svgIcon.style.display = 'none';
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.style.backgroundImage = 'none';
            preview.classList.remove('w-64', 'h-64');
            svgIcon.style.display = 'block';
        }
    });
</script>
