<x-app-layout>
    <div class="m-6">
        <x-alerta/>

        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6">
            @foreach ($noticias as $noticia)
                <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700 overflow-hidden transition-transform duration-500 ease-out hover:scale-105 relative flex flex-col">
                    <div class="absolute top-0 right-0 p-2 bg-white bg-opacity-75 rounded-md">
                        <p class="text-xs text-gray-500">{{ $noticia->created_at->diffForHumans() }}</p>
                    </div>
                    <a href="#">
                        <img class="w-full h-48 object-cover" src="{{ asset('storage/fotos/koala.jpg') }}" alt="" />
                    </a>

                    <div class="p-5 flex-grow">
                        <a href="#">
                            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{{ $noticia->titulo }}</h5>
                        </a>
                        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">{{ Str::limit($noticia->contenido, 100, '...') }}</p>
                    </div>
                    <div class="p-5">
                        <div class="flex justify-center items-center space-x-2">
                            <a href="{{ route('back.noticias.show', $noticia) }}" class="inline-flex justify-center items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-500 rounded-lg hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-400 dark:hover:bg-blue-500 dark:focus:ring-blue-600">
                                Ver más
                            </a>
                            <a href="{{ route('back.noticias.edit', $noticia) }}" class="inline-flex justify-center items-center px-3 py-2 text-sm font-medium text-center text-white bg-green-500 rounded-lg hover:bg-green-600 focus:ring-4 focus:outline-none focus:ring-green-300 dark:bg-green-400 dark:hover:bg-green-500 dark:focus:ring-green-600">
                                Editar
                            </a>
                            <button data-modal-show="modalborrado-{{ $noticia->id }}" class="inline-flex justify-center items-center px-3 py-2 text-sm font-medium text-center text-white bg-red-500 rounded-lg hover:bg-red-600 focus:ring-4 focus:outline-none focus:ring-red-300 dark:bg-red-400 dark:hover:bg-red-500 dark:focus:ring-red-600">
                                Borrar
                            </button>
                        </div>
                    </div>
                </div>

                <x-modalborrado
                    idModal="modalborrado-{{ $noticia->id }}"
                    mensaje="¿Estás seguro de que quieres borrar esta noticia?"
                    ruta="{{ route('back.noticias.destroy', $noticia) }}"
                />
            @endforeach
        </div>
    </div>

    @pushn('js')
        <script>
            document.addEventListener('DOMContentLoaded', () => {
                const showModal = (modalId) => {
                    document.getElementById(modalId).classList.remove('hidden');
                };

                const hideModal = (modalId) => {
                    document.getElementById(modalId).classList.add('hidden');
                };

                document.querySelectorAll('[data-modal-show]').forEach(button => {
                    button.addEventListener('click', () => {
                        showModal(button.getAttribute('data-modal-show'));
                    });
                });

                document.querySelectorAll('[data-modal-hide]').forEach(button => {
                    button.addEventListener('click', () => {
                        hideModal(button.getAttribute('data-modal-hide'));
                    });
                });
            });
        </script>
    @endpushn
</x-app-layout>
