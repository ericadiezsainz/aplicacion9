<x-app-layout >
    <div class="container mx-auto p-6 mt-5 bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
        <form action="{{ route('back.noticias.update', $noticia) }}" method="POST"  enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="space-y-12">
                <div class="border-b border-gray-900/10 pb-12">
                    <h2 class="text-base font-semibold leading-7 text-gray-900">Noticias</h2>
                    <p class="mt-1 text-sm leading-6 text-gray-600">Crear una noticia y lo vera todo el mundo.</p>

                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                    <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div class="sm:col-span-4">
                            <label for="titulo" class="block text-sm font-medium leading-6 text-gray-900">Titulo</label>

                            <div class="mt-2">
                                <div class="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                    <input type="text" name="titulo" id="titulo" value="{{ old('titulo', $noticia->titulo) }}" class="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                    @error('titulo')
                                        <p style="color: #dc3545;"> {{ $message }} </p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="col-span-full">
                            <label for="contenido" class="block text-sm font-medium leading-6 text-gray-900">Contenido</label>
                            <div class="mt-2">
                                <textarea id="contenido" name="contenido" rows="3" class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">{{ old('contenido', $noticia->contenido) }}</textarea>
                            </div>

                            <p class="mt-3 text-sm leading-6 text-gray-600">Escribe la noticia</p>
                        </div>
                    </div>

                    <div class="col-span-full gap-y-8 mt-7">
                        <label for="cover-photo" class="block text-sm font-medium leading-6 text-gray-900">Cover photo</label>
                        <div class="mt-2 flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10">
                            <div class="text-center">
                                <div id="preview" class="bg-cover w-full h-60">
                                    <img id="preview-image" class="w-full h-60" src="{{ asset('storage/' . $noticia->foto ) }}">
                                </div>

                                <div class="mt-4 flex text-sm leading-6 text-gray-600">
                                    <label for="foto" class="relative cursor-pointer rounded-md bg-white font-semibold text-indigo-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-indigo-500">
                                        <div class="flex flex-col items-center justify-center">
                                            <span>Subir Imagen</span>
                                            <p class="text-xs leading-5 text-gray-600">PNG, JPG, up to 1MB</p>
                                        </div>
                                        <input type="file" id="foto" name="foto" class="sr-only">
                                    </label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-6 flex justify-center gap-x-6">
                    <button type="submit" class="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</x-app-layout>

<script>
    document.getElementById('foto').addEventListener('change', function(e) {
        var file = e.target.files[0];
        var reader = new FileReader();
        var preview = document.getElementById('preview');
        var previewImage = document.getElementById('preview-image');

        if (file) {
            reader.onloadend = function() {
                previewImage.src = reader.result;
                previewImage.style.display = 'block';
                preview.style.backgroundImage = 'none';
            }
            reader.readAsDataURL(file);
        }
        else {
            previewImage.style.display = 'none';
            preview.style.backgroundImage = 'url("' + previewImage.src + '")';
            preview.style.backgroundSize = 'cover'; // Asegura que la imagen se ajuste al contenedor
        }
    });
</script>
