@if (session('mensaje'))
    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 mb-6 rounded relative" role="alert">
        <span class="block sm:inline">{{ session('mensaje') }}</span>
    </div>
@endif
