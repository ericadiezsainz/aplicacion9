@props([
    'idModal',
    'mensaje',
    'ruta',
])

<div {{ $attributes->merge(['class' => 'fixed inset-0 z-50 flex justify-center items-center overflow-y-auto overflow-x-hidden hidden']) }} id="{{ $idModal }}">
    <div class="fixed inset-0 bg-black opacity-50" data-modal-hide="{{ $idModal }}"></div>
    <div class="relative p-4 max-w-2xl w-full">
        <div class="relative bg-white dark:bg-gray-700 rounded-lg shadow-lg overflow-hidden">
            <button type="button" data-modal-hide="{{ $idModal }}" class="absolute top-3 right-3 text-gray-400 hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-2 dark:hover:bg-gray-600 dark:hover:text-white">
                <svg class="w-4 h-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                </svg>
            </button>
            <div class="p-6 text-center">
                <svg class="mx-auto mb-4 text-gray-400 w-12 h-12 dark:text-gray-200" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m0-4h.01M12 2a10 10 0 100 20 10 10 0 000-20z"/>
                </svg>
                <h3 class="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">{{ $mensaje }}</h3>
                <form action="{{ $ruta }}" method="POST" class="inline-flex space-x-2">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-red-600 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 dark:focus:ring-red-800 rounded-lg">
                        Sí, estoy seguro
                    </button>
                </form>
                <button type="button" data-modal-hide="{{ $idModal }}" class="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-4 focus:ring-gray-100 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:text-white dark:focus:ring-gray-700">
                    No, cancelar
                </button>
            </div>
        </div>
    </div>
</div>
