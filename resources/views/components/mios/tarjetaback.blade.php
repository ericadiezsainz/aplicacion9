@props([
    'registro',
    'modelo',
    'titulo' => '',
    'modalMensaje' => 'No hay mensaje',
])

<div class="container mx-auto p-6 mt-5">
    <div class="max-w-6xl mx-auto bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
        <ul class="flex border-b">
            <li class="-mb-px mr-1">
                <a class="bg-white inline-block border-l border-t border-r rounded-t py-2 px-4 text-gray-500">{{ $titulo }} {{ $registro->id }}</a>
            </li>
            <li class="ml-auto">
                <a class="bg-white inline-block py-2 px-4 text-gray-400 font-semibold hover:text-blue-500" href="{{ route('back.' . $modelo . '.index') }}">Volver</a>
            </li>
        </ul>
        <div class="flex items-center justify-center">
            <div class="w-1/3">
                <img class="rounded-lg w-4/5 h-auto mx-auto" src="{{ asset('storage/fotos/koala.jpg') }}" alt="Imagen de la noticia" />
            </div>
            <div class="w-2/3 p-6">
                <div class="flex items-center justify-between">
                    <h2 class="text-xl font-semibold leading-7 text-gray-900">{{ $registro->titulo }}</h2>
                </div>
                <p class="mt-3 text-base leading-6 text-gray-700">{{ $registro->contenido }}</p>
                <div class="mt-6 text-sm text-gray-500">
                    @foreach ([
                        ['icon' => 'M4 6h16M4 10h16M4 14h16M4 18h16', 'text' => 'Fecha de creación: ' . $registro->created_at->format('d/m/Y H:i')],
                        ['icon' => 'M6 18L18 6M6 6l12 12', 'text' => 'Última actualización: ' . $registro->updated_at->format('d/m/Y H:i')],
                        ['icon' => 'M12 14l9-5-9-5-9 5 9 5z', 'text' => 'Autor: ' . $registro->user->name],
                    ] as $item)
                        <div class="flex items-center">
                            <svg class="h-4 w-4 mr-1.5 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="{{ $item['icon'] }}"></path>
                            </svg>
                            <p>{{ $item['text'] }}</p>
                        </div>
                    @endforeach
                </div>

                @auth
                    <div class="mt-6 flex space-x-4">
                        <a href="{{ route('back.' . $modelo . '.edit', $registro) }}" class="inline-flex justify-center items-center px-3 py-2 text-sm font-medium text-center text-white bg-green-500 rounded-lg hover:bg-green-600 focus:ring-4 focus:outline-none focus:ring-green-300 dark:bg-green-400 dark:hover:bg-green-500 dark:focus:ring-green-600">
                            Editar
                        </a>
                        <button data-modal-show="modalborrado-{{ $registro->id }}" class="inline-flex justify-center items-center px-3 py-2 text-sm font-medium text-center text-white bg-red-500 rounded-lg hover:bg-red-600 focus:ring-4 focus:outline-none focus:ring-red-300 dark:bg-red-400 dark:hover:bg-red-500 dark:focus:ring-red-600">
                            Borrar
                        </button>
                    </div>
                @endauth
            </div>
        </div>
    </div>
</div>

@auth
    <x-modalborrado
        idModal="modalborrado-{{ $registro->id }}"
        mensaje="{{ $modalMensaje }}"
        ruta="{{ route('back.noticias.destroy', $registro) }}"
    />
@endauth
