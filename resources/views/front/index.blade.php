<x-main-layout>
    @foreach ($noticias as $noticia)
        <a href="{{ route('front.noticias.show', $noticia) }}" class="container mx-auto">
            <div class="max-w-6xl mx-auto bg-white border border-gray-300 rounded-lg shadow-md hover:shadow-lg transition duration-300 ease-in-out hover:border-gray-400 hover:bg-gray-50 relative">
                <div class="absolute top-0 right-0 p-2 bg-gray-200 bg-opacity-75 rounded-md border border-gray-300">
                    <p class="text-xs text-gray-700">{{ $noticia->created_at->diffForHumans() }}</p>
                </div>
                <div class="flex items-center">
                    <div class="w-full md:w-1/3 overflow-hidden">
                        <img class="rounded-l-lg md:rounded-l-none md:rounded-r-none w-full h-auto" src="{{ asset('storage/fotos/koala.jpg') }}" alt="Imagen de la noticia" />
                    </div>
                    <div class="w-full md:w-2/3 p-6">
                        <div class="flex items-center justify-between">
                            <h2 class="text-2xl font-semibold leading-7 text-gray-900">{{ $noticia->titulo }}</h2>
                        </div>
                        <p class="mt-3 text-base leading-6 text-gray-700">{{ Str::limit($noticia->contenido, 150, '...') }}</p>
                        <div class="mt-4 text-sm text-gray-600 flex items-center justify-between">
                            <div>
                                <svg class="h-4 w-4 mr-1.5 text-gray-400 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 14l9-5-9-5-9 5 9 5z"></path>
                                </svg>
                                <p class="inline-block">Autor: {{ $noticia->user->name }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    @endforeach
</x-main-layout>
